import { Component } from '@angular/core';
import {Todo} from './models/todo';
import {datatodo} from './savedata';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent {
  title = 'demo';

  todos = ['Windstorm', 'Bombasto', 'Magneta', 'Tornado'];
  addTodo(newTodo: string) {
    if (newTodo) {
      this.todos.push(newTodo);
    }
  }
}
